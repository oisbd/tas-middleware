<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Schema;
use DB;
use Session;
use Redirect;

class GetUserLogInfo extends Controller
{
    public function getUserInfo()
    {
        $sqlsrvConn = DB::connection(env('Default_Conncetion'));
        $sqliteConn = DB::connection('sqlite');
        

        $getData = [];
        $data = [];
        DB::connection(env('Default_Conncetion'))->enableQueryLog();

        // get current minute
        $currentMinutes = date('i');

        /*
        status:
            0 = initial (default)
            1 = valid user
            2 = invalid user
        ProcessStatus:
            0 = initial (default)
            1 = ready to process or process pending
            2 = middleware process failed
            3 = rows successfully processed

        middleware pick data where status=2 and ProcessStatus=2 in every hour's first minute else every time pick data/row where status=0 and ProcessStatus=1

        */
        $dynamic_status = ((int) $currentMinutes === 1 ? 2 : 0);
        $dynamic_process = ((int) $currentMinutes === 1 ? 2 : 1);

        // block for zkteco
        if (env('vendor_name') == "zkteco"){
            if(env('Default_Conncetion') == 'sqlsrv'){
                $table_name_inout = 'CHECKINOUT';
                $table_name_user_info = 'USERINFO';
                $user_id = 'USERID';
                $user_name = 'NAME';
                $check_time = 'CHECKTIME';
                $device_no = 'cio.sn';
                $vendor_type = 2;
                $status = 'Status';
                $process_status = 'process_status';              
            // end sqlite db operation          	 
        } else if(env('Default_Conncetion') == 'mysql')
            {
            	print_r(env('Default_Conncetion'));exit;

            } else {
                return response(['status'=>"error",'message'=>"Unauthrised database"]);
            }// checking default database
            
        }// zktech block end
        // block for actatek start
        else if(env('vendor_name') == "actatek")
        {
            if(env('Default_Conncetion') == 'sqlsrv')
            {
                return response(['status'=>"error",'message'=>"Actatek Device"]);
                
            }else{
                return response(['status'=>"error",'message'=>"Connection Error"]);
            }

        }else if(env('vendor_name') == "anviz"){
            if(env('Default_Conncetion') == 'sqlsrv')
                {
                    $table_name_inout = 'Checkinout';
                    $table_name_user_info = 'UserInfo';
                    $user_id = 'Userid';
                    $user_name = 'Userid';
                    $check_time = 'CheckTime';
                    $device_no = 'NULL';
                    $vendor_type = 3;
                    $status = 'Status';
                    $process_status = 'ProcessStatus';
                                
            }else{
                return response(['status'=>"error",'message'=>"Connection Error"]);
            }
        } else {
            return response(['status'=>"error",'message'=>"No Vendor find"]);
        }// chekcing vendor name        

        // update process status 1 40 rows order by time asc
        DB::update(DB::raw(";WITH CTE AS 
        ( SELECT TOP 40 *  FROM  $table_name_inout WHERE $status=0 ORDER BY $check_time ) 
        UPDATE CTE SET $process_status=1"));

        /*
         * $local_ip_address provide client device's IP address
         * */
        $local_ip_address = $_SERVER['REMOTE_ADDR'];

        /*
         * Get top 40 data from sql server.
         * All table name and field/column name comes from if else block based on vendor name
         * */
        $getData = $sqlsrvConn->table("$table_name_inout as cio")
                ->join("$table_name_user_info as ui","ui.$user_id","=","cio.$user_id")
                ->where("cio.$status",$dynamic_status)
                ->where("cio.$process_status",$dynamic_process)
                ->select(DB::raw("cio.$user_id as user_id,ui.$user_name as user_name,convert(varchar,cio.$check_time,20)as check_time,$device_no as device_no, $vendor_type as vendor_type, NULL as status"))
                ->orderBy("cio.$user_id")
                ->limit(40)
                ->get()->toArray();
        echo '<pre>';
        print_r($getData);
        echo '</pre>';
        exit;
        

        if(empty($getData)){
            return response(['status'=>"error",'message'=>"No data found for processing"]);
        }// check $getData empty or not

        // get auth key from sqlite db  
        $is_logged = Session::get('is_logged');
        $getAuthKey = $sqliteConn->table('db_config')
                        ->select('*')
                        ->where('log_id',$is_logged)
                        ->first();

        if(empty($getAuthKey)){
            Session::put('error','Database not configured yet!');
            return redirect::to('/dashboard');
        }
        $requestData = json_encode($getData);

        /*
         * prepare data to store in sqlite api call log
         * */
        $requestedLog =[
            'institute_id' => NULL,
            'request_log' => $requestData,
            'response_log' => NULL,
            'request_time' => date("Y-m-d h:i:s"),
            'response_time' => NULL,
            'ip_address' => $_SERVER['HTTP_HOST'],
            'api_url' => NULL,
            'auth_key' => $getAuthKey->auth_key,
        ];

        /*
         * insert requested data as json in sqlite table name api_call_log
         * */
        $last_id = $sqliteConn->table('api_call_log')
                            ->insertGetId($requestedLog);

        /*
        ================================
               API performance
        ================================
        */
        require'../vendor/autoload.php';
        $baseUrl=env('base_url');
        // Create a client with a base URI
        $client = new Client(['base_uri'=>$baseUrl, 'timeout'  => 2.0]);
        //make uri which will connect with base uri in url bar
        $uri = $baseUrl.env('api_uri');
        // Send a request to API or call to API
        $request = $client->request("POST",$uri,
        [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ],
            'query' => [
                'data' => $getData,
                'auth_key'=>$getAuthKey,
                'remote_ip'=>$local_ip_address
            ]
        ]);

        //get response from API call
        $return_result = json_decode($request->getBody(),true);

        if(empty($return_result)){
            return response(['status'=>"error",'message'=>"No response get from API"]);                 
        }
        $response = json_encode($return_result);

        /*
         * $updateResponseLog variable execute the statement for api call log update
         * for last inserted id
         * */
        $updateResponseLog = $sqliteConn->table('api_call_log')
                                ->where('id',$last_id)
                                ->update([
                                    'response_log' => $response,
                                    'response_time'=> date('Y-m-d H:i:s')
                                ]);

        $count =0;
        /*
         * through the foreach statement system update the rows
         * which we selected before and made them process
         * status=1 for valid user and status=2 for invalid user
         * */
        foreach ($getData as $key => $value) {            
            $value->status = $return_result[$value->user_name];
            $pro_status = $value->status === 1 ? 3 : 2;
            $updateLog = $sqlsrvConn->table("$table_name_inout")
                    ->where("$user_id", $value->user_id)
                    ->where("$check_time", $value->check_time)
                    ->update([
                        "$status" => $value->status,
                        "$process_status" => $pro_status
                    ]);
            if($updateLog){
                $count++;
            }
        }// end update foreach of response

        if($count === count($getData)){
            Session::put('message','Log data successfully inserted but some data are not. Please check log table.');
            return Redirect::to('/dashboard');
        }else{
            Session::put('error','Data not inserted to system');
            return Redirect::to('/dashboard');
        }        
    }
}
