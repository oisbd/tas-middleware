<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Redirect;

class AgentLoginController extends Controller
{
    public function index()
    {
        $log_id = Session::get('is_logged');
        if(empty($log_id)){
            return view('login');
        }
        return redirect::to('/dashboard');

    }

    // login check
    public function userLogin(Request $r)
    {
        
        if(isset($r->submit) == 'submit')
        {
            $username = $r->username;
            $password = $r->password;

            $sqliteConn = DB::connection('sqlite');

            if($sqliteConn)
            {
                $getUser = $sqliteConn->table('user')
                        ->select('username', 'password', 'status')
                        ->where('username',$username)
                        ->where('password',$password)
                        ->where('status',1)
                        ->first();
                // print_r($getUser);exit;
                if(empty($getUser))
                {
                	return Redirect::to('/');                    
                }
                Session::put('logged_user',$username);                   
                Session::put('is_logged',1);                
                return Redirect::to('/dashboard');
                
            }else{
                return Redirect::to('/');
            }
        }
    }// login check end


    // Logout function
    public function logout(){
        Session::flush();
        return Redirect::to('/');
    }// logout end
}
