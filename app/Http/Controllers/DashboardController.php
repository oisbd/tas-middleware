<?php

namespace App\Http\Controllers;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Http\Request;
use Redirect;
use Session;
use DB;

class DashboardController extends Controller
{
    public function index()
    {
    	$is_logged = Session::get('is_logged');
        if($is_logged != 1){
            return Redirect::to('/');
        }
        $sqliteConn = DB::connection('sqlite');
        /*
         * if user previously stroed data for db configuration
         * then after login in dashboard all data are shown in web form
         * and user can modify it not insert again unless totally erase/delete data
         * */
        $getDbConf = $sqliteConn->table('db_config')
                        ->select('*')
                        ->where('log_id',$is_logged)
                        ->first();
        return view('middleware.dashboard', compact('getDbConf'));           
    }

     // store db config
    public function storeDbConfig(Request $request)
    {
        /*
         * server side validation if user passing any of form field is empty
         * */
        $validatedData = $request->validate([
            'auth_key' => 'required',
            'vandor_name' => 'required',
            'db_conn' => 'required',
            'db_host' => 'required',
            'db_port' => 'required',
            'db_name' => 'required'
        ]);
        $is_logged = Session::get('is_logged');
        $sqliteConn = DB::connection('sqlite');

    	$configData =[            
            'auth_key'      => (string)$request->auth_key,
            'vendor_name' 	=> (string)$request->vandor_name,
            'DB_CONNECTION' => (string)$request->db_conn,
            'DB_HOST' 		=> (string)$request->db_host,
            'DB_PORT' 		=> (int)$request->db_port,
            'DB_DATABASE' 	=> (string)$request->db_name,
            'DB_USERNAME' 	=> (string)$request->db_username,
            'DB_PASSWORD'   => (string)$request->db_password,
            'log_id' 	=> (string)$is_logged
        ];        
        
        if($sqliteConn){
        $checkData = $sqliteConn->table('db_config')
                    ->select('*')
                    ->where([
                            'auth_key'      => (string)$request->auth_key,
                            'vendor_name'   => (string)$request->vandor_name,
                            'DB_CONNECTION' => (string)$request->db_conn,
                            'DB_HOST'       => (string)$request->db_host,
                            'DB_PORT'       => (int)$request->db_port,
                            'DB_DATABASE'   => (string)$request->db_name,
                            'DB_USERNAME'   => (string)$request->db_username,
                            'DB_PASSWORD'   => (string)$request->db_password,
                            'log_id'    => $is_logged
                        ])
                    ->first();

            if($checkData != NULL){
                Session::put('warn','No data modified.');
                return redirect::to('/dashboard');
            }else{
                foreach($configData as $key => $value){
                    $this->changeEnvironmentVariable($key,$value);
                }
                /*
                 * if default connection is sql server then
                 * dynamically add two columns in
                 * */

                if(env('Default_Conncetion') == 'sqlsrv'){
                    if (!\Schema::connection(env('Default_Conncetion'))->hasColumn('Checkinout','Status')) {
                        Schema::connection(env('Default_Conncetion'))->table('Checkinout', function (Blueprint $table) {
                            $table->integer('Status')->default(0)->after('OpenDoorFlag');
                        });
                    }
                }
                if(env('Default_Conncetion') == 'sqlsrv'){
                    if (!\Schema::connection(env('Default_Conncetion'))->hasColumn('Checkinout','ProcessStatus')) {
                        Schema::connection(env('Default_Conncetion'))->table('Checkinout', function (Blueprint $table) {
                            $table->integer('ProcessStatus')->default(0)->comment('0 = unprocessed, 1 = processed, 2 = pending')->after('Status');
                        });
                    }
                }
                if(empty($request->id)){
                    $insert = $sqliteConn->table('db_config')
                              ->insert($configData);            
                    Session::put('message','Data inserted successfully');
                    return redirect::to('/dashboard');
                }else{
                    $update = $sqliteConn->table('db_config')
                            ->where('log_id','=',$is_logged)
                            ->update($configData);
                    Session::put('message','Data successfully updated');
                    return redirect::to('/dashboard');
                }              
            }            
        }
    }

    public static function changeEnvironmentVariable($key,$value)
    {
        $path = base_path('.env');

        if(is_bool(env($key)))
        {
            $old = env($key)? 'true' : 'false';

        }else if(env($key)===null){
            $old = 'null';
        }else{
            $old = env($key);
        }        
        if (file_exists($path)) {
            file_put_contents($path, str_replace(
                "$key=".$old, "$key=".$value, file_get_contents($path)
            ));
        }
    }
}
