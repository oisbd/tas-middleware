$(document).ready(function(){
    // $.ajaxSetup({
    //     headers: {
    //        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    //     }
    // });
    $( "#db_config" ).on('submit',function(e) {
        e.preventDefault();
        var formData = $( this ).serialize();
        // console.log(formData);return;

        $.ajax({
                url: "/save-config",
                type: 'POST',
                data: formData, 
                dataType:'JSON',
                success: function (result) {
                    swal("Saved", "This resource was added to your list of saved resources", "success")
                },
                error: function (result) {
                }
        });  
    });
});