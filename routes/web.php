<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/user-login','AgentLoginController@userLogin');
Route::group(['middleware' => 'revalidate'], function()
{
    Route::get('/','AgentLoginController@index');
    Route::get('/dashboard','DashboardController@index');
    Route::post('/db-config','DashboardController@storeDbConfig');
	Route::get('/logout','AgentLoginController@logout');
	Route::get('/log-info','GetUserLogInfo@getUserInfo');
});


