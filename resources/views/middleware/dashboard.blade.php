<!DOCTYPE html>
<html>
<head>
	<title>Middleware Dashboard</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css" rel="stylesheet">

	<!-- custome css -->
	<link rel="stylesheet" type="text/css" href="{{ url('frontEnd/css/dashboard.css') }}">
</head>
<body>    
<div class="container">
  <div class="dashboard">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <ul class="dashboard-tabs">
                <li class="active">
                    <a href="{{ url('/dashboard') }}" class="btn" aria-controls="profile" role="tab" data-toggle="tab">
                        <span class="glyphicon glyphicon-wrench"></span>
                        <h4>Environment Setup</h4>
                    </a>
                </li>
                {{--<li style="float: center">--}}
                    {{--<a href=""><img src="{{ url('frontEnd/images/fingerprint_logo.png') }}" width="50px" height="78px" alt="team-fingerprint"></a>--}}
                    {{--<br>--}}
                    {{--<h3 class="text-primary text-center">Fems TAS Middleware</h3>--}}
                {{--</li>--}}
                <!-- <li>
                    <a href="" class="btn" aria-controls="statistics" role="tab" data-toggle="tab">
                        <span class="glyphicon glyphicon-time"></span>
                        <h4>Report</h4>
                    </a>
                </li>
                <li>
                    <a href="{{ url('/log-info') }}" class="btn" aria-controls="settings" role="tab" data-toggle="tab">
                        <span class="glyphicon glyphicon-cog"></span>
                        <h4>Process Log</h4>
                    </a>
                </li> -->
                <li style="float: right">
                    <a href="{{ url('/logout') }}" class="btn" aria-controls="help" role="tab" data-toggle="tab">
                        <span class="glyphicon glyphicon-log-out"></span>
                        <h4>Logout</h4>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            
            <div class="card">
              <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <?php
                  $msg = Session::get('message');
                  $error = Session::get('error');
                  $warn = Session::get('warn');
                  if($msg){
                ?>
                  <div class="alert alert-success">
                    <strong>Success!&nbsp;{{ $msg }}</strong>
                  </div>
                <?php
                    Session::put('message',null);
                  }else if($error){
                ?>
                  <div class="alert alert-danger">
                    <strong>Error:&nbsp;{{ $error }}</strong>
                  </div>
                <?php
                   Session::put('error',null);
                  }else if($warn){
                ?>
                  <div class="alert alert-warning">
                    <strong>Warning:&nbsp;{{ $warn }}</strong>
                  </div>
                <?php
                   Session::put('warn',null);
                  }
                ?>
                <fieldset>
                    <legend>Environment Setup</legend>
                    <form method="post" action="{{ url('/db-config') }}">
                        <input type="hidden" name="_token" value='{{ csrf_token() }}'>
                        <input type="hidden" name="id" value="{{ isset($getDbConf->id)? $getDbConf->id : '' }}">
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-2 col-form-label">Authentication Key</label>
                            <div class="col-sm-10">
                              <input type="text" name="auth_key" class="form-control" id="auth_key" value="{{ isset($getDbConf->auth_key)? $getDbConf->auth_key : '' }}" placeholder="authentication key" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Vandor Name</label>
                            <div class="col-sm-10">
                              <select class="form-control" name="vandor_name" >
                                @if(isset($getDbConf->vendor_name))
                                  <option value="{{ $getDbConf->vendor_name }}">{{$getDbConf->vendor_name}}</option>
                                @endif
                                  <option value="">select vandor name</option>
                                  <option value="zkteco">ZKTeco</option>
                                  <option value="actatek">Actatek</option>
                                  <option value="anviz">Anviz</option>
                              </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Database Connection</label>
                            <div class="col-sm-10">
                              <select class="form-control" name="db_conn" >
                                @if(isset($getDbConf->DB_CONNECTION))
                                  <option value="{{ $getDbConf->DB_CONNECTION }}">{{ $getDbConf->DB_CONNECTION }}</option>
                                @endif
                                  <option value="">select db connectin</option>
                                  <option value="mysql">MySql</option>
                                  <option value="access">MS-Access</option>
                                  <option value="sqlsrv">SQL Server</option>
                                  <option value="sqlite">SQLite</option>
                              </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-2 col-form-label">Host/Server/Path</label>
                            <div class="col-sm-10">
                              <input type="text" name="db_host" class="form-control" id="host_name" value="{{ isset($getDbConf->DB_HOST)? $getDbConf->DB_HOST : '' }}" placeholder="db host/server name" >
                              <small>For: sql server (PC-Name/Sql server name), mysql (127.0.0.1), sqlite (database/database.sqlite) etc</small>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-2 col-form-label">DB Port</label>
                            <div class="col-sm-10">
                              <input type="number" name="db_port" class="form-control" id="db_port" value="{{ isset($getDbConf->DB_PORT)? $getDbConf->DB_PORT : '' }}" placeholder="e.g. 1433" >
                              <small>For: sql server(1433), mysql(3306) etc</small>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-2 col-form-label">Database Name</label>
                            <div class="col-sm-10">
                              <input type="text" name="db_name" class="form-control" id="db_name" value="{{ isset($getDbConf->DB_DATABASE)? $getDbConf->DB_DATABASE : '' }}" placeholder="Database name" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-2 col-form-label">Username</label>
                            <div class="col-sm-10">
                              <input type="text" name="db_username" class="form-control" id="db_username" value="{{ isset($getDbConf->DB_USERNAME)? $getDbConf->DB_USERNAME : '' }}" placeholder="database username" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-2 col-form-label">Database Password</label>
                            <div class="col-sm-10">
                              <input type="password" name="db_password" class="form-control" id="db_pass" value="{{ isset($getDbConf->DB_PASSWORD)? $getDbConf->DB_PASSWORD : '' }}" placeholder="database password" >
                            </div>
                        </div>
                        <div class="text-right">
                        @if(isset($getDbConf->log_id))
                          <button type="submit" class="btn btn-md btn-success"><span class="glyphicon glyphicon-save"></span>&nbsp;Update</button>
                        @else
                          <button type="submit" class="btn btn-md btn-success"><span class="glyphicon glyphicon-save"></span>&nbsp;Save</button>
                        @endif
                          <button type="reset" class="btn btn-md btn-danger"><span class="glyphicon glyphicon-remove"></span>&nbsp;Reset</button>
                        </div>
                    </form>
                </fieldset>                
              </div>
            </div>
            {{--<p style="text-align: center;">Powered by <a href="#">Fingerprint Ltd.</a></p>--}}
            <p style="text-align: center;">Copyright &copy; 2019 <a href="teamfingerprint.com">Fingerprint Ltd.</a> All Rights Reserved</p>
        </div>        
    </div>
  </div>
</div>



<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<!-- <script type="text/javascript" src="{{ url('frontEnd/js/dashboard.js') }}"></script> -->
</body>
</html>