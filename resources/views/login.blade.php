<!DOCTYPE html>
<html>
<head>
	<title>User Login</title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

	<!-- custome css -->
	<link rel="stylesheet" type="text/css" href="{{ url('frontEnd/css/style.css') }}">
</head>
<body>

<div class="wrapper fadeInDown">
  <div id="formContent">
    <div class="fadeIn first">
      <img src="{{ url('frontEnd/images/database.jpg') }}" id="icon" alt="User Icon" />
    </div>

    <!-- Login Form -->
    <form method="post" action="{{ url('/user-login') }}">
      {{ csrf_field() }}
      <input type="text" id="login" class="fadeIn second" name="username" placeholder="username">
      <input type="password" id="password" class="fadeIn third" name="password" placeholder="password">
      <input type="submit" name="submit" class="fadeIn fourth" value="Log In">
    </form>
    <p style="text-align: center;">Powered by <a href="teamfingerprint.com">Fingerprint Ltd.</a></p>
    {{--<p style="text-align: center;">Copyright &copy; 2019 <a href="#">Fingerprint Ltd.</a> All Rights Reserved</p>--}}
  </div>
</div>


<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</body>
</html>